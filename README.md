# Checkout and donation form for Sangha

## Components

### \<sangha-material-checkout\>


## Development

```bash
# Get dependencies
$ npm install

# Demo site
$ npm start

# Run tests
$ npm test
```
